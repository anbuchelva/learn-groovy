// arithmatic operators
assert 10 + 1 == 11
assert 10 - 1 == 9
assert 10 * 1 == 10
assert 10 / 2 == 5
assert 10 % 3 == 1
assert 10 ** 2 == 100

def a = 10
a += 5
assert  a == 15

// relational operators
assert  1 + 2 == 3
assert 3 != 4
assert 3 < 4
assert 4 > 3
assert 3 >= 2
assert 3 <= 4

// logical operators
assert !false
assert true && true
assert true || false

// conditional operators
String s = "a"
if (s != null && s.length() > 0) {
    result = "Found"
} else {
    result = "Not found"
}
println result

// the above code can be written in a short form
result = s.length() > 0 && s != null ? "Found":"Not found"
println result

// if user.name available displa user.name or display anonymous
displayName = user.name ? user.name : "Anonymous"
// this can be shorted further
displayName = user.name ?:"Anonymous"

// Objects operators
// Java

person p = new Person()
if (p.address = null) {
    Address address = p.address
}
// safe navigation operator when the value is null
def address = p?.address // this checks whether the address is null
assert address == null

