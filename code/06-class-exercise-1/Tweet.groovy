@groovy.transform.ToString()
class Tweet {
    String userid
    Date tweetdate
    String message
    Integer likes
    Integer retweets

    Tweet(String user, String tweet) {
        userid = user
        message = tweet
        likes = 0
        retweets = 0
        TweetDate = new Date()
    }

    void addRetweets() {
        retweets += 1
    }

    void addLikes() {
        likes += 1
    }

}
