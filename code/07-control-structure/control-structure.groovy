// if
if ( true )
  println "value is true"

// false | null | empty strings | empty collections
if ( false )
  println "value is false"  //this wouldn't get printed as groovy doesn't go to next step if it is false

String name = null
if ( name )
  println "name has no value" // similar to false

String last = ""
if ( last )
  println "last has value" // empty string also excluded

//-----------------------------------------------------------------------

// if and else
def x = 0
if (x == 10){
  println "x is 10"
} else {
  println "x is not 10"
}

//-----------------------------------------------------------------------
// while loop
def i = 1
while ( i <= 10 ) {
  println i
  i++
}

//-----------------------------------------------------------------------
// for in list
def list = [1,2,3,4]
for ( num in list ) {
  println num
}

//-----------------------------------------------------------------------
// clousure
def list2 = [1,2,3,4]
list2.each { println it }

//-----------------------------------------------------------------------
// list
def myNumber = 10
switch(myNumber) {
  case 1:
    println "number is 1"
    break
  case 2:
    println "number is 2"
    break
  default:
    println "we hit the default case"  
}
