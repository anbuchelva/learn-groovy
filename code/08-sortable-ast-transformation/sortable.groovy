import groovy.transform.Sortable
import groovy.transform.ToString
import org.apache.ivy.plugins.conflict.StrictConflictException

@ToString
@Sortable
class Person{
    String first
    String last
}

p1 = new Person(first: "Tamil", last: "Selvan")
p2 = new Person(first: "Anbu", last: "Selva")


def people = [p1, p2]
println people

println "--- providing false will sort temporarily and store in new variable"
def sorted_people = people.sort(false)
println sorted_people
println people

println "--- not providing false will sort the list permanently"
def sorted_people_perm = people.sort()
println sorted_people_perm
println people