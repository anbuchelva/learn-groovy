// create a new instance of class developer
Developer d = new Developer()
d.first = "Anbu"
d.setLast("Selvan") // can use either of the one method

// assign some language
// `d.languages.class` displays the data type
d.languages << "Groovy"
d.languages << "Python"
d.languages << "GoLang"

// call some methods
d.work()

// execute this with the following command
// `groovyc app.groovy`, this will create app.class and Developer.class and display the output of work method
