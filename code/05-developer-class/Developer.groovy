@groovy.transform.ToString()
class Developer{

    String first
    String last
    def languages = [] //not mandatory to specify the data type

    // work method
    void work(){
        println "$first $last is working"
    }

}
