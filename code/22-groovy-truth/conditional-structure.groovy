// if ( boolean expression ) { logic }

if ( true ) {
    println 'true'
}
 
// if one line no need to put {}
if (true)
    println 'true'
    
def age = 35
if ( age >= 35 ) {
    println "can run for president"
}

// if -- else
if (true) {
    println "true"    
} else {
    println "false"
}

def yourage = 20
if ( yourage >= 21 ){
    println "buy beer"
} else {
    println "no beer for you"
}

// if -- else if -- else
if ( false ) {
    println "false"
} else if (true) {
    println "true"
} else {
    println "final else"
}

def someage = 37
if ( someage >= 21 && someage < 35 ){
    println "buy beer"
} else if ( someage >= 35 ) {
    println "you can run for president"
} else {
    println "you are too young"
}
