//Looping

//While loop
List numbers = [1,2,3]
while (numbers) {
    numbers.remove(0)
}

println numbers

//for loop

//for (variable  in iterable){
//    
//}
println"---"
List nums = [1,2,3]
for (Integer i in nums) {
    println i
}
println"---"
for (i=0; i<=10; i++){
    println i
}
println"---"
for (i in 1..5) {
    println i
}
println"---"
//return / break / condinue

String getFoo() {
    "foo"
}

getFoo()

println"---"

a = 1
while (true) {  // infinite loop
    a++
    break
}

println a

println"---"

for (String s in 'a'..'z'){
    if (s == 'a') continue // if s == a, will not do nothing and continue run the next code
    println s
    if (s > 'b') break // if s > b break the code
}

println"---"