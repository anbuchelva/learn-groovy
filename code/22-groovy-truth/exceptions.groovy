//Exceptions

def foo() {
    throw new Exception("Foo exception")
}

List log = []

try {
    foo()
} catch(Exception e) {  //catch generic exception
    log << e.message
    println e.class.name  // class type
    println e.message // exception message
} finally {
    log << 'finally' // add finally
}


println log
