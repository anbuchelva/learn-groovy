//switch statemnt for multiple ifs
def num = 9
//def num = 4.toFloat()
switch (num) {
    case 1:
        println "1"
        break;
    case 2:
        println "2"
        break;
    case 1..3:
        println "in range 1..3"
        break;
    case [1,2,12]:
        println "num is in list [1,2,12]"
        break;
    case Integer:
        println "num is an integer"
        break;
    case Float:
        println "num is a float"
        break;
    default:
        println "default"
}

// in
def validAges = 18..35
def someAge = 19
println someAge in validAges