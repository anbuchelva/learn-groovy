//yes if condition is met no if not met
def name = "Anbu"
isitanbu = (name.toUpperCase() == 'ANBU') ? 'Yes':'No'
println isitanbu

//check if the message is not null
def msg
def output = (msg != null) ? msg: 'default message...'

// check the left side is null
def elvisOutput = msg ?: 'default message..'

println msg
println output
println elvisOutput
