class Person {
    String firstName, lastName    
    def dob
    // private | protected | public
    protected String f1, f2, f3
    private Date createdOn = new Date()
    
    static welcomeMsg = "Hello!"
    public static final String WELCOME_MSG = "HELLO" // final static cannot be changed
    
    def foo(){
        String msg = "hello"
        String firstName = "Anbu" // this will not collaid with the first name createdin line #2
        println "$msg, $firstName"
    }   

}


println Person.welcomeMsg  // we can directly call the static msg 

Person p = new Person()
println p.foo()