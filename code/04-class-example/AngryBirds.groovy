/**
* usually capitalize the first letter of a class like AngryBirds
* groovy file without a class called script
* a groovy file can contain more than 1 class
* running `groovyc AngryBirds.groovy` would create 3 different files like AngryBirds.class, Bird.class and Pig.class
*/

class AngryBirds{

}

class Bird{

}

class Pig {

}