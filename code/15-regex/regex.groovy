import java.util.regex.*

Pattern pattern = Pattern.compile("abc")
println pattern
println pattern.class

// Groovy
// the forward slash doesn't bother the escape scting i.e., the backslash
String slashy = /a\b/
println slashy

// the text between $/ /$ is considered as text, though there are forward slashes are being used
String url = $/https://anbuchelva.in//$
println url

// groovy makes it simple to use the regex patterns
def pattern1 = ~/a\b/
println pattern1.class

//The same to be written in Java using the following
Pattern pattern3 = Pattern.compile("a\\\\c")
println pattern3
println pattern3.class

def text = "Being a Cleveland Sports Fan is no way to go through life!"
def pattern4 = ~/Cleveland Sports Fan/

// to see the pattern is within the text
def finder = text =~ pattern4
println finder
println finder.size() // returns the number of times that the pattern matches 

// to see the pattern is exactly matching with the text
def pattern5 = ~/Being a Cleveland Sports Fan is no way to go through life!/
def matcher = text ==~ pattern5
println matcher

// may use the matcher in the in condition to replace something in the text string
if (matcher) {
    text = text.replaceFirst(pattern4, "Buffalo Sports Fan")
    println text
}