def number = 10
println number.class

def decimal = 5.5
println decimal.class

// converting data types
//----------------------
// explicit casting
// define data type as float instead of big decimal
def myFloat = (float) 1.0
println myFloat.class

// rules of +-*/
Float f = 5.25
Double d = 10.5
def result = d / f
println result
println result.class
// the result will be Double in groovy

Float a = 1.1
Float b = 2.2
def result2 = b / a
println result2
println result2.class
// though both a and b are being flot the output will be double

def c = 150.0 //bigdecimal
def e = 15
result3 = c / e
println result3
println result3.class

result4 = 5.0d - 4.1d
println result4
println result4.class

result5 = 5 - 4.1
println result5
println result5.class

// integer division
def intDiv = 1/2
println intDiv
println intDiv.class
println 1.intdiv(2)

// GDK Methods
// -----------
assert 2 == 2.5.toInteger() //conversion
assert 2 == 2.5 as Integer  // enforced coercion
assert 2 == (int) 2.5       // cast

assert '5.50'.isNumber()    // conversion
assert 5 == '5'.toInteger() // conversion to integer

// times | upto | downto | step

20.times{
    print '-'
}

println '\n'

1.upto(10){ num ->
    println num
}

println '\n'

10.downto(1){ num1 ->
    println num1
}

println '\n'

0.step(1,0.1) { num2 ->
    println num2
}
