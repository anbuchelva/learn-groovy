// doc link: http://docs.groovy-lang.org/latest/html/groovy-jdk/java/util/List.html

List sample_list = [1, 2, 3]
println sample_list

// add | remove | get | clear

sample_list.push(4)
println sample_list

// replace (0 - postion, 100 - replace with)
sample_list.putAt(0,100)
println sample_list

// replace position 0 with 200
sample_list[0] = 200
println sample_list

// add values to list temporarily or to create a new list from existing list
println sample_list + [4, 5, 6]
println sample_list  // original list is not modified

// add item at the end of the list
sample_list << 300
println sample_list

// remove the first value
sample_list.pop()
println sample_list

// remove specific index
sample_list.removeAt(1)
println sample_list

// remove the value 300
println sample_list - 300 // this is temp
println sample_list

// get index 2
println sample_list[2]

// change the values of the entire list
sample_list = ["x", "y", 1, 2, 3, 3, 4, 5, 6, 7, 8, 9, 10]
// get values from index range
println sample_list.getAt(0..3)

// clear
sample_list.clear()
// or
sample_list = []
println sample_list

sample_list = ["x", "y", 1, 2, 3, 3, 4, 5, 6, 7, 8, 9, 10]
for (a in sample_list) {
    print "list index value:" + a + "\n"
}

// add a list inside a list
sample_list << [3, 4, 5]
sample_list << [1, 2]
println sample_list

for (a in sample_list) {
    print "list index value:" + a + "\n"
}

// flatten the list - by removing the list inside a list
println sample_list.flatten()

println sample_list
// display only the unique values
println sample_list.unique()

// this also removes the duplicates
def sample_set = sample_list as Set
println sample_set

// remove duplicates and sort - it should contain same data type i.e., x, y are not applicable
def sorted_set = [1, 2, 3, 3, 4, 5, 6, 7, 8, 9, 10, 10, 0] as SortedSet
println sorted_set
