/*
If you are new to Java or Groovy the idea of an Enum might be new. An Enum is a collection of constant values. We can use this collection of constants to create ranges. I want you to do some reading up on enum's and create an enum for days of the week. ex Sunday, Monday, etc...

  -  Create a range from that enum
  -  Print the size of the Range
  -  Use the contains method to see if Wednesday is in that Range
  -  Print the from element of this range
  -  Print the to element of this range
*/

enum Days {
    Sunday, 
    Monday, 
    Tuesday, 
    Wednesday, 
    Thursday, 
    Friday, 
    Saturday
}

def dayRange = Days.Sunday..Days.Saturday
println "$dayRange"

// Size of the range
println "Size of the range: " + dayRange.size()

// check Wednesday is there in the range using `contains`
println dayRange.contains(Days.Wednesday)

// Print the from element of this range
println dayRange.From

// Print the to element of this range
println dayRange.To

// --------------------------------------------------------

/*
Lists

  -  Create a list days (Sun - Sat)
  -  Print out the list
  -  Print the size of the list
  -  Remove Saturday from the list
  -  Add Saturday back by appending it to the list
  -  Print out the Wednesday using its index
*/

List days_list = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
//println days_list.class

println days_list
println days_list.size()
days_list.remove("Saturday")
println days_list
days_list.add("Saturday")
println days_list
println days_list[3]

// ----------------------------------------------------------
/* 
Maps

  -  Create a map of days of the week  
        - 1: Sunday, 2:Monday, etc...
  -  Print out the map
  -   Print out the class name of the map
  -  Print the size of the map
  -  Is there a method that would easily print out all of the days (values)?   
        - Without closures you may have to look at the Java API for LinkedHashMap
*/

days_map = [1: "Sunday", 2: "Monday", 3: "Tuesday", 4:"Wednesday", 5:"Thursday", 6:"Friday", 7:"Saturday"]
println days_map
println days_map.size()
days_list2 = []
for (day in days_map.keySet()) {
//    println days_map[day]
    days_list2.add(days_map[day])
}
println days_list2
println days_map.values() // another method
