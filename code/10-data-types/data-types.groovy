byte b = 20
B = b.getClass().getName()
println B

short s = 1000
S = s.class
println S

float f = 1.25
F = f.class
println F

// groovy automatically deducts the data type
println 3.class
println 378945646546464.class
println 3789456465464649874646546465465.class

println 4.50.class

// need to declare the variable and data type in Java
// if we need to declare a vaiable use `def` command; no need to assign data type
def x = 10
println "date type of x: " + x.class

// the data type automatically changes, based on the data
x = "abcd"
println "date type of x: " + x.class

//declare the data type of the variable stg
String stg = "abcd"
println "date type of stg: " + stg.class