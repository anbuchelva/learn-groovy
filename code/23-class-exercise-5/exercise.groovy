class Account {
    BigDecimal balance = 0.0

    def deposit(BigDecimal amount){
        if (amount < 0) {
            throw new Exception("Deposit amount must be greater than 0")
        }
        balance += amount

    }
    def deposit(List amounts){
        for (amount in amounts) {
            deposit(amount)
        }

    }
}


Account checking = new Account()
checking.deposit(10)
println checking.balance
println "---"

// Try depositing negative value and catch the error
try{
    checking.deposit(-10)
} catch (Exception e) {
    println e.message
}
println checking.balance
println "---"

// Try deposit the values from a list positive values will be added and negative values will be ignored.
try{
    checking.deposit([1,5,10,20,-60])
} catch (Exception e) {
    println e.message
}
println checking.balance
