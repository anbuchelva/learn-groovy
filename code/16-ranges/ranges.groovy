Document link: http://docs.groovy-lang.org/latest/html/gapi/groovy/lang/Range.html
// for loops for range

for (int x = 1; x <= 10; ++x){
    print x
}

println ("")

for (int y = 10; y >= 1; --y){
    print y
}

println ("")

def letters = ['a', 'b', 'c']
for (int i = 0; i < letters.size(); ++i){    
    print letters[i]    
}


println ("")

// Range

Range r = 1..10
println "$r"
println r.class
println r.from
println r.to

// create range.to < 101
Range r1 = 1..<101
println "$r1"

Range l = 'a'..'z'
println "$l"

assert (0..10).contains(0)
assert (0..10).contains(10)
assert (0..10).contains(-1)  == false  //this should fail

//-------------------------------------------------------------

Date today = new Date()
Date oneWeekAway = today + 7
println today
println oneWeekAway

Range days = today..oneWeekAway
println "$days"
