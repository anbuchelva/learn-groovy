#!/home/anbuchelva/.sdkman/candidates/groovy/current/bin/groovy
println "hello from shebang line"

/**
* this is to run a groovy script directly from terminal just like a shell script
* change the file permission to execute by `chmod +x shebang.groovy`
* note: groovy should be installed already to run it in terminal.
*
* #!/home/anbuchelva/.sdkman/candidates/groovy/current/bin/groovy - is the path where groovy is installed
* the above line to be changed accordingly E.g., #!/usr/bin/env groovy
*/