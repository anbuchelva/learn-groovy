// this is a single line comment

def message = "hello world"  //comments can be placed here

/*
this is a multiline comment
2nd line for example
*/

/**
* it is called as groovy doc comment
* there are tools to read the comments and generate documentation
* this is class person
*/
class Person{
    
}