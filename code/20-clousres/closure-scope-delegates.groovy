class ScopeDemo {

    def outerClosure = {
        println this.class.name
        println owner.class.name
        println delegate.class.name
        def nestedClosure = {
            println "Nested-" + this.class.name
            println "Nested-" + owner.class.name
            println "Nested-" + delegate.class.name
        }
        nestedClosure()
    }

}
def demo = new ScopeDemo()
demo.outerClosure()

println "---"

// Delegate
def writer = {
    append "Anbu"
    append " lives in Chennai"
}

def append(String s) {
    println "append() called with argument of $s"
}

StringBuffer sb = new StringBuffer()
// The following line swaps the default option
writer.resolveStrategy = Closure.DELEGATE_FIRST
writer.delegate = sb
writer()

