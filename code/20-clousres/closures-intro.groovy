def c = { }
println c.class.name
println c instanceof Closure

// call the method
def sayHello = {
    println "Hello"
}
sayHello()

// call method with parameters
sayHello = { name ->
    println "Hello $name"
}
sayHello('Anbu')

List nums = [1, 4, 7, 4, 30, 2]
nums.each({
    println it
})

println "----------------------------"

nums.each({num ->
    println num
})

println "----------------------------"

def timesTen(num, closure) {
    closure(num * 10)
}

timesTen(5, {println it})

timesTen(554654) {
    println it
} // for making the code reading easy

println "----------------------------"

10.times{
    println it
}

println "----------------------------"

import java.util.Random

Random rand = new Random()
// print 3 random numbers every time the code is run
3.times{
    println rand.nextInt()
}

76.times{
    print "-"
}

println("\n")
