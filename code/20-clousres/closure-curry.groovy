// curry
def log = { String type, Date createdOn, String msg ->
    println "$createdOn [$type] - $msg"
}

log("DEBUG",new Date(),"this is a sample debug statement")
log("DEBUG",new Date(),"this is a sample debug statement")
log("DEBUG",new Date(),"this is a sample debug statement")

def debugLog = log.curry("DEBUG")
debugLog(new Date(), "This is created using curry")

def todayDebugLog = log.curry("DEBUG", new Date())
todayDebugLog("this is even simple.")

println "---"
// right curry
def crazyPersonLog = log.rcurry("Why am I logging this way")
crazyPersonLog("ERROR", new Date())

println "---"
// index based curry
def typeMsgLog = log.ncurry(1, new Date())
typeMsgLog("ERROR", "This is created using ncurry")
