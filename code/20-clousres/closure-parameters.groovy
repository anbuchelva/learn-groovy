// implicit parameter
def foo = {
    println it
}

foo('Anbu')  // it will always print the parameter that is given

/*
def noparams = { ->
    println it
}

noparams(1)  // there is no closure to accept an arguement

*/


// multiple arguements are given with comma
def sayHello  = { String first, String last ->
    println "Hello, $first $last"
}

sayHello("Anbu","Selvan")

// default value if we don't want to pass
def greet = { String name, String greeting = "Howdy" ->
    println "$greeting, $name"
}
greet("Anbu","Hello")  // if we pass the value, it will take this value
greet("Selvan") // if we don't pass any value, it will take the default one

// var-arg
def concat = { String... args ->
    args.join('-')
}
println concat("abc","def",'123')

// get the property of a closure
def someMethod(Closure c) {
    println "..."
    println c.maximumNumberOfParameters
    println c.parameterTypes
}

def someClosure = { int x, int y ->
    x+y
}

someMethod(concat)
someMethod(greet)
someMethod(sayHello)
someMethod(someClosure)