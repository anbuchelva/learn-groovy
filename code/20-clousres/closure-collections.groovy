// each & each with index
List favNums = [2, 545, 54,78, 82, 74]

for (num in favNums) {
    println num
}

println("---")

// same output with closure
favNums.each{ println it}

println("---")
// using for loop print index and values
for (int i=0; i < favNums.size(); i++ ) {
    println "$i:${favNums[i]}"
}
println("---")
// using eachWithIndex method
favNums.eachWithIndex { num, idx ->
    println "$idx:$num"
}

println("---")
// findAll method
List days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
List weekends = days.findAll {
    it.startsWith("S")
}

println days
println weekends

println("---")
// collect method
List nums2 = [1,5,6,4,6,7,8,4,7,3,9]

numTimesTen = []
// looping through all items in list
nums2.each { num ->
    numTimesTen << num * 10
}

println nums2
println numTimesTen

// get the same output using collect
List num2TimesTen = nums2.collect{ num ->
    num * 10
}
println num2TimesTen
