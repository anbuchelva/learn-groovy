// map in groovy: http://docs.groovy-lang.org/latest/html/groovy-jdk/java/util/Map.html

def map1 = [:]
println map1
println map1.getClass().getName()

def person = [first: "Anbu", last: "Selvan", email:"anbu@example.com" ]
println person

// access the key from the above map
println person.first

// add a new key value
person.twitter = "@anbuchelva"
println person

// use quotes for spaces between key
def twitter = [username:"@anbuchelva", 'email address':"anbu@example.com"]
println twitter

// using key
def createdDate = "Account Created Date"
twitter.createdDate = "01/01/2010" // w/o key
println twitter

twitter.(createdDate) = "01/01/2010" // with key
println twitter

// Can't sort the map, but can sort by keys
println twitter.sort()

// looping through person
for ( entry in twitter ) {
    println entry
}

// print only the key
for ( key in twitter.keySet()){
    println key
}

// print only the key an value
for ( key in twitter.keySet()){
    println "$key:${twitter[key]}"
}

// each | eachWithIndex
