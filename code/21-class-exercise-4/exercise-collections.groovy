/*
Explore the GDK

In the following exercises we are going to explore the GDK to find some methods that take closures and learn how to use them. Hint - I would narrow your search to java.util.Collection, java.lang.Iterable & java.util.List

- Search for the find and findAll methods.
    - What is the difference between the two? 
    - Write some code to show how they both work.
- Search for the any and every methods.
    - What is the difference between the two? 
    - Write some code to show how they both work. 
- Search for the method groupBy that accepts a closure
    - What does this method do? 
    - Write an example of how to use this method.
*/

List people = [
    [name: "Anbu", city:"Chennai"],
    [name: "Muthu", city:"Bangalore"],
    [name: "Saravanan", city:"Chennai"],
    [name: "Senthil", city:"Bangalore"]
]

//find method
println people.find { person ->
    person.city == "Chennai"
}

//findAll
println people.findAll { person ->
    person.city == "Bangalore"
}

//any
println people.any { person -> 
    person.city == "Chennai"
}

//every - check every person from chennai
println people.every { person ->
    person.city == "Chennai"
}

//check every person's name length is greater than or equal to 5
println people.every { person ->
    person.name.size() >= 5
}


//groupby
def peopleByCity = people.groupBy { person ->
    person.city
}
println peopleByCity

def from_chennai = peopleByCity["Chennai"]
def from_Bangalore = peopleByCity.Bangalore

from_chennai.each {
    println it.name
}

from_Bangalore.each {
    println it.name
}
