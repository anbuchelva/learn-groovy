//Create a Method that accepts a closure as an argument
//Create a closure that performs some action
def mymethod1(Closure x) {
    println "Create a closure that performs some action"
}
mymethod1()

// Call the method and pass the closure to it
def mymethod2(Closure c){
    c()
}

def foo = {
    println "Call the method and pass the closure to it"
}
mymethod2(foo)

//Create a list and use them each to iterate over each item in the list and print it out
List names = ["Anbu", "Selvan", "Dan", "Vega"]
names.each { name ->
    println name
}

//Create a map of data and iterate over it using each method.
Map my_info=[first: "Anbu", Last:"Selvan", email:"anbu@example.com"]
my_info.each { info->
    println info
}

println my_info

my_info.each {a, b ->
    println "$a:$b"
}

//Demonstrate the use of curry and try to come up with an example different from the one we used in the lecture.
def greet = { String greeting, String name ->
    println "$greeting, $name"
}

greet("Hello", "Anbu")

def sayHello = greet.curry("Hello")
sayHello("Selvan")
