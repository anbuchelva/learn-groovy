// run this program by opening it in groovyConsole using command `groovyConsole assertions.groovy`
// then select each of the line and Script > Run Selection or Ctrl + Shift + R

// you must provide an assertion an expression that evaluates to true
assert true

// we can provide a full expression on the right hand sige
// note that unlike Java and more like Ruby or Scala == is equality
assert 1 == 1

// like the example above we are evaluating an expression
def x = 1
assert x == 1

// what happens when the expression doesn't evaluate to true?
assert fase

// The power assertion output shows evaluation results from the outer to the inner expression
assert 1 == 2
/*
assert 1 == 2
         |
         false
*/

// complex debug output
assert 1 == (3+10)*100/5*20
/*
assert 1 == (3+10)*100/5*20
         |    |   |   | |
         |    13  1300| 5200
         false        260
*/


def x = [1, 2, 3, 4, 5]
assert [x << 6] == [6, 7, 8, 9, 10]
/*
assert [x << 6] == [6, 7, 8, 9, 10]
        | |     |
        | |     false
        | [1, 2, 3, 4, 5, 6]
        [1, 2, 3, 4, 5, 6]
*/