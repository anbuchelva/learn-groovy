// create our own class to add two different accounts

class Account {
    BigDecimal balance
    Account plus(Account other) {
        new Account(balance: this.balance + other.balance)
    }
    String toString(){
        "Account Balance: $balance"
    }
}

Account savings = new Account (balance: 100.0)
Account checking = new Account (balance: 500.0)

println savings
println checking
println savings + checking
