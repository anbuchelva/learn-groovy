def a = 100
def b = 3
println "Concatenate: " + a + b
println "Plus: " + a.plus(b)
println "Minus: " + a.minus(b)
println "Multiply: ": + a.multiply(b)
println "Div: " + a.div(b)
println "mod: " + a.mod(b)
println "Power: " + a.power(b)
println "OR: " + a.or(b)
println "And: " + a.and(b)
println "XOR:" + a.xor(b)

s1 = "Hello,"
s2 = "Groovy!"
println s1 + s2
println s1.plus(s2)
