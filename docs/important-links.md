# Links

## Downloads
JDK Downloads: https://www.oracle.com/java/technologies/javase-downloads.html

Groovy Download: http://groovy-lang.org/download.html

sdkman: https://sdkman.io/

## Groovy executables
groovysh: http://groovy-lang.org/groovysh.html

groocyc: http://groovy-lang.org/groovyc.html

groocyConsole: http://groovy-lang.org/groovyconsole.html

IntelliJ IDEA: https://www.jetbrains.com/idea/

## Imports
groocy default imports: http://groovy-lang.org/structure.html#_default_imports

## Keywords
java keywords: https://docs.oracle.com/javase/tutorial/java/nutsandbolts/_keywords.html

groovy keywords: http://groovy-lang.org/syntax.html#_keywords

## Groovy Syntax / Semantics
groovy comments: http://groovy-lang.org/syntax.html#_single_line_comment

power assertions: http://groovy-lang.org/semantics.html#_power_assertion

groovy class: http://groovy-lang.org/objectorientation.html#_class

groovy control structure: http://groovy-lang.org/semantics.html#_control_structures

groovy annotations: http://groovy-lang.org/objectorientation.html#_annotation

groovy api: https://docs.groovy-lang.org/docs/latest/html/gapi/

groovy api package summary: http://docs.groovy-lang.org/next/html/gapi/groovy/transform/package-summary.html

groovy operators: https://groovy-lang.org/operators.html

regular expressions - quick reference: https://www.regular-expressions.info/refquick.html

range: http://docs.groovy-lang.org/latest/html/gapi/groovy/lang/Range.html

list: http://docs.groovy-lang.org/latest/html/groovy-jdk/java/util/List.html

map: http://docs.groovy-lang.org/latest/html/groovy-jdk/java/util/Map.html

## Closures
api : http://docs.groovy-lang.org/latest/html/api/groovy/lang/Closure.html

doc: http://groovy-lang.org/closures.html
