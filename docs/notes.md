# Notes

* dynamic language similar to python and Java
* Option for static typing and static compiling
* Optionally typed
* `groovysh` - groovy shell, where we can write groovy scripts and test on screen
* `:h` would give help
* `:clear` would clear the screen
* `groovyc` - this would compile the groovy script to class, then the class can be executed
* `groovyConsole` - this would launch a separate application, where we can write / save / open groovy scripts and run
* `Ctrl+r` to run the script
* `Ctrl+w` to clear the output
* `Ctrl+/` to comment a line
* `Ctrl+Shift+r` - to run selected code
* groovy compiles the script into class then execute the code using run method.
* all classes and methods are public
* no need to mention return for class/functions
* semi colons are not needed
* no need to have getter / setter
* groovy includes both groovy and java keywords
* value.getClass().getName() - gives the data type
* value.class - also gives the data type

## Java Primitive Data types
Primitive Type | Wrapper Type | Description |
|---|---|---|
|byte|java.lang.Byte|8 bit signed int|
|short|java.lang.Short|16 bit signed int|
|int|java.lang.Integer|32 bit signed int|
|long|java.lang.Long|64 bit signed int|
|float|java.lang.Float|single precision (32 bit) floating point|
|double|java.lang.Double|single precision (64 bit) floating point|
|char|java.lang.Character|16 bit Unicode Character|
|boolean|java.lang.Boolean|true or false|

* Groovy uses the wrapper class that comes with Java

## Regular Expressions
* `=~` - find operator
* `==~` - match operator
* `~string` - string operator

Samples:
|Pattern | Meaning
|---|---|
|`abc`|matches any string contains `a` followed by `b` and followed by `c`|
|`b[aeiou]t`|matches `bat`, `bet`, `bit`, `bot` and `but`|
|`<TAG\b[^>]*>(.*?)</tag>`|matches the opening and closing pair of specific HTML tag|
|`^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$`|matches any email address|

more is available here: https://www.regular-expressions.info/refquick.html

## Closures

What are closures used?

- Iterators
- callbacks
- Higher-order functions
- Specialized Control Structure
- Builders
- Resource allocation
- Threads
- DSLs
- Fluent Interfaces

### `owner`, `delegate` and `this` in closure
**this:** corresponds to the enclosing class where the closure is defined.
**owner:** corresponds tot he enclosing object where the closure is defned, which may be either a class or closure.
**delegate:** correcponds to the third party object where methods calls or properties are resolved whenever the receiver of the messageis not defined.

## Evaluating Boolean Tests
|Runtime Type|Evaluation Criteria Required for Truth|
|---|---|
|Boolean|Boolean value is true|
|Matcher|Matcher has Match|
|Collection|Collection is non empty|
|Map|Map is non empty|
|String|String is non empty|
|Number, Character|Number is non zero|
|None of the above|Object Referece is non null|


